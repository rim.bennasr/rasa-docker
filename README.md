# Dockerfile and Docker-compose.yml  for rasa bot framework


## Technologies 
Project Created with : 
* docker 
* docker-compose 

## Examples
### To build the image run: 
```
$ cd rasa-docker/
$ sudo docker build -t rimbns/rasa .
```
## To run the containers: 
Docker-compose: 
```
$ cd rasa-docker/   
$ sudo docker-compose up
```

### To run rasa server :
```
$ sudo docker run -it  --name rasa_server -p 5005:5005 -v $(pwd):/home/app rimbns/rasa run \
--model your_model.tar.gz
```

### To run rasa server linked to action server :
```
$ sudo docker run -it -p 5005:5005  --link action_server -v $(pwd):/home/app rimbns/rasa run
```


### To run action server :
```
$ sudo docker run -it --name action_server -p 5055:5055 -v $(pwd):/home/app rimbns/rasa run actions 
```


### To train a model:
```
$ sudo docker run -it -p 5005:5005 -v $(pwd):/home/app rimbns/rasa train \
--out models/
```


## Notice 
* If you are using UFW on ubuntu , enable forwarding in UFW for docker to function correctly 
and get network inside the containers.

* dont forget to change your action_endpoint in  endpoints.yml

here's rasa link if you want to know more: 
https://rasa.com/docs/rasa/user-guide/how-to-deploy/#running-the-rasa-server
