FROM python:3.7 
MAINTAINER  Rim Ben Nasr "rim.bennasr@protonmail.com"
ENV REFRESHED_AT 2020-02-20

RUN echo "nameserver 8.8.8.8" > /etc/resolv.conf

#the virtual env is disabled as the container 
#is handling the isolation part
#WORKDIR /home/venv
#creating virtualenv to isolate the builds 
#RUN python -m venv --system-site-packages ./venv-rasa 

#downloading rasa related packages and dependencies 
RUN apt-get update && pip install -U pip 
RUN pip install rasa --default-timeout=100 
RUN pip install rasa[spacy] \
       	&& python -m spacy download fr_core_news_md --default-timeout=100 \
	&& python -m spacy link  fr_core_news_md fr 

#setting the working directory to /home/app
WORKDIR /home/app

#volume for temporary data 
VOLUME /tmp

#exposing the port 5005 for rasa server && 
#the port 5055 for custom action server
EXPOSE 5005
EXPOSE 5055

ENTRYPOINT ["rasa"]
CMD ["--help"]
